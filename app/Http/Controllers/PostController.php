<?php

namespace App\Http\Controllers;
use App\Http\Resources\Post as PostResource;
use App\friend;
use App\Http\Resources\PostCollection;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $friends = Friend::friendships();
        if($friends->isEmpty()){
            return new PostCollection(request()->user()->posts);
        }
        return new PostCollection(
            Post::whereIn('user_id',
                [
                    $friends->pluck('user_id'),
                    $friends->pluck('friend-id')
                ]
            )->get()
        );
    }

    public function store(){
        $data = request()->validate([
           'body'=>'',
        ]);
        $post = request()->user()->posts()->create($data);
        return new PostResource($post);

    }

}
